from django.contrib import admin
from django.urls import path, include                 
from rest_framework import routers                    
from backend import views                            

router = routers.DefaultRouter()                      
router.register(r'games', views.GameView, 'game')
router.register(r'players', views.PlayerView, 'player')

urlpatterns = [
            path('admin/', admin.site.urls),         path('api/', include(router.urls))                
            ]