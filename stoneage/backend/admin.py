from django.contrib import admin
from .models import Game
from .models import Player

class GameAdmin(admin.ModelAdmin):
    list_display = ('id', 'turn', 'number_of_players', 'user_stories', 'knowledge_points', 'team_points')


class PlayerAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'color', 'game')

# Register your models here.
admin.site.register(Game, GameAdmin)
admin.site.register(Player , PlayerAdmin)
