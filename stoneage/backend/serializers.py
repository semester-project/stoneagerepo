from rest_framework import serializers
from .models import Game 
from .models import Player 

class GameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = ('id', 'turn', 'number_of_players', 'user_stories', 'knowledge_points', 'team_points')

class PlayerSerializer(serializers.ModelSerializer):
    game = serializers.StringRelatedField()

    class Meta:
        model = Player
        fields = ('id', 'name', 'color', 'game')