from django.db import models

# Create your models here.
class Game(models.Model):
    turn = models.IntegerField(default=0)
    number_of_players = models.IntegerField(default=0)
    user_stories = models.IntegerField(default=0)
    knowledge_points = models.IntegerField(default=0)
    team_points = models.IntegerField(default=0)

class Player(models.Model): 
    name = models.CharField(max_length=120)
    color = models.CharField(max_length=120)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)

class Product(models.Model):
    name = models.CharField(max_length=120)
    tier = models.IntegerField(default=0)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)