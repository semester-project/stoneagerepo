from django.shortcuts import render
from rest_framework import viewsets
from .serializers import GameSerializer 
from .serializers import PlayerSerializer
from .models import Game
from .models import Player

# Create your views here.

class GameView(viewsets.ModelViewSet):
    serializer_class = GameSerializer
    queryset = Game.objects.all()

class PlayerView(viewsets.ModelViewSet):
    serializer_class = PlayerSerializer
    queryset = Player.objects.all()
