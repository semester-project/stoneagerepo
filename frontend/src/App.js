import React, { Component } from "react";
import './App.css';
import NewGame from './Components/NewGame';
import AddPlayers from './Components/AddPlayers';
import PlayGame from './Components/PlayGame';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gameStarted: false,
      playersAdded: false
    };
  }

  handleChange() {
    // Comment and uncomment following line to change gameStarted value
    // this.setState({gameStarted: true});

    // Comment and uncomment following line to change playersAdded value
    this.setState({playersAdded: true});
  }

  render() {
    // Determining what to render based on state
    let component;
    if (this.state.playersAdded) {
      component = <PlayGame />;
    }
    else if (this.state.gameStarted) {
      component = <AddPlayers />;
    }
    else {
      component = <NewGame onChange={this.handleChange()}/>;
    }
    return (
      <div className="App">
        <header className="App-header">
          {component}
        </header>
      </div>
    );
  }
}

export default App;