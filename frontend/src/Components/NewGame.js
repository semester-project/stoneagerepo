import React, { Component } from "react";
import axios from "axios";
import {
    Button,
    Form,
    FormGroup,
    Input,
    Label
} from "reactstrap";

export default class NewGame extends Component {
    constructor(props) {
        super(props);
        this.state = {
            game: {
                id: "",
                turn: "0",
                number_of_players: "",
                user_stories: "0",
                knowledge_points: "0",
                team_points: "0"
            }
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = e => {
        let {name, value} = e.target;
        const game = { ...this.state.game, [name]: value }
        this.setState({game});
    }

    handleSubmit() {
        axios
            .post(`http://localhost:8000/api/games/`, this.state.game);
    }

    render() {
        return (
            <div>
                <h1>Scrum Stoneage</h1>
                <FormGroup>
                    <Form>
                        <Label>
                            Number of Players:
                        <Input
                                type="text"
                                name="number_of_players"
                                defaultValue={this.state.game.number_of_players}
                                onChange={this.handleChange}
                            />
                        </Label>
                    </Form>
                </FormGroup>
                <Button onClick={() => this.handleSubmit()}>New Game</Button>
            </div>
        );
    }
}