import React, { Component } from "react";
import axios from "axios";
import {
    Button,
    Form,
    FormGroup,
    Input,
    Label
} from "reactstrap"

export default class AddPlayers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            player: {
                id: "",
                name: "",
                color: "",
                game_id: "1"
            }
        }
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = e => {
        let { name, value } = e.target;
        const player = { ...this.state.player, [name]: value }
        this.setState({ player });
    }

    handleSubmit() {
        axios
            .post(`http://localhost:8000/api/players/`, this.state.player);
    }

    render() {
        return (
            <div>
                <h1>Add Players</h1>
                <FormGroup>
                    <Form>
                        <Label>
                            Name:
                        <Input
                                type="text"
                                name="name"
                                defaultValue={this.state.player.name}
                                onChange={this.handleChange}
                            />
                        </Label>
                    </Form>
                </FormGroup>
                <FormGroup>
                    <Form>
                        <Label>
                            Color:
                            <Input type="select" name="color" onChange={this.handleChange}>
                                <option value="Red">Red</option>
                                <option value="Blue">Blue</option>
                                <option value="Yellow">Yellow</option>
                            </Input>
                        </Label>
                    </Form>
                </FormGroup>
                <Button onClick={() => this.handleSubmit()}>Add Player</Button>
            </div>
        );
    }
}