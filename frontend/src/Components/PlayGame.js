import React, { Component } from "react";
import axios from "axios";
import {
    Container,
    Row,
    Col,
    Button,
} from "reactstrap";

export default class PlayGame extends Component {
    constructor(props) {
        super(props);
        this.state = {
            players: [
                // Get players from props somehow, inserting dummy data for now
                "Ashley",
                "Ben",
                "Brett",
                "Nathaniel",
                "Randall"
            ],
            turn: 1,
            user_stories: 0,
            knowledge_points: 0,
            team_points: 0
        };
    }

    gainUserStory() {
        var stories = this.state.user_stories;
        stories++;
        this.setState({
            user_stories: stories
        });
    }

    workOnUserStory() {
        var stories = this.state.user_stories;
        stories--;
        this.setState({
            user_stories: stories
        });
    }

    sprintReview() {
        var knowledge = this.state.knowledge_points;
        knowledge++;
        this.setState({
            knowledge_points: knowledge 
        });
    }

    endTurn() {
        var player = this.state.turn;
        if(player === this.state.players.length) {
            player = 1;
        }
        else {
            player++;
        }
        this.setState({
            turn: player 
        });
    }

    sprintRetrospective() {
        var team = this.state.team_points;
        team++;
        this.setState({
            team_points: team 
        });
    }

    render() {
        return (
            <div>
                <Container>
                    <Row>
                        <Col>Player's Turn: {this.state.players[(this.state.turn - 1)]}</Col>
                    </Row>
                </Container>
                <Container className="pt-5">
                    <Row>
                        <Col>
                            <h1>Backlog Area</h1>
                            <p>Gather requirements from your customer</p>
                            <p>Current User Stories: {this.state.user_stories}</p>
                            <Button onClick={() => this.gainUserStory()}>Gain User Story</Button>
                        </Col>
                        <Col>
                            <h1>Development Area</h1>
                            <p>Work on user stories from the backlog</p>
                            <p>Current User Stories: {this.state.user_stories}</p>
                            <Button onClick={() => this.workOnUserStory()}>Work on a User Story</Button>
                        </Col>
                    </Row>
                    <Row className="pt-5 pb-5">
                        <Col>
                            <h1>Sprint Review Area</h1>
                            <p>Review your progress to gain knowledge points</p>
                            <p>Current Knowledge Points: {this.state.knowledge_points}</p>
                            <Button onClick={() => this.sprintReview()}>Hold a Sprint Review</Button>
                        </Col>
                        <Col>
                            <h1>Sprint Retrospective Area</h1>
                            <p>Review your workflow to gain team points</p>
                            <p>Current Team Points: {this.state.team_points}</p>
                            <Button onClick={() => this.sprintRetrospective()}>Hold a Sprint Retrospective</Button>
                        </Col>
                    </Row>
                </Container>
                <Container className="pt-5">
                    <Row>
                        <Col>
                            <Button onClick={() => this.endTurn()}>End Turn</Button>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}